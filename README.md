# Chat App
Node JS App that allows users to access chat rooms and communicate with others users in real time through the use of Socket IO.

The Application starts in the Join page where users are required to input their chosen username as well as the Chat Room that they would want to join. 

*Note: Only users in the specific chat room can communicate with other users. Usernames must be unique per Chat Room.*

## Setup
Install the node modules by running
```
npm install

```

Run the server by running:

**Production**
```
npm run start
```

**Dev**
```
npm run dev
```

Access the web application on your browser
```
http://localhost:3000/
```
